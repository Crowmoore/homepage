(ns homepage.views.main
  (:require
   [cljs-react-material-ui.core :refer [get-mui-theme]]
   [cljs-react-material-ui.reagent :refer [mui-theme-provider]]
   [homepage.views.frontpage :refer [frontpage-view]]))

(defn main-view []
  [mui-theme-provider
   {:mui-theme (get-mui-theme)}
   [frontpage-view]])