(ns homepage.views.frontpage
  (:require
   [cljsjs.material-ui]
   [cljs-react-material-ui.core :refer [get-mui-theme color]]
   [cljs-react-material-ui.reagent :as ui]
   [cljs-react-material-ui.icons :as ic]
   [homepage.styles :as style]
   [reagent.core :as reagent :refer [atom as-element]]))

(defn frontpage-view []
  (fn []
      [:div {:style (merge style/banner style/centered)}
       [ui/avatar {:size "250"
                   :src "http://cdn.countrysidenetwork.com/wp-content/uploads/how-to-raise-sheep.jpg"}]
       [:h1 "Joel Kortelainen"]]))