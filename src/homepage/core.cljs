(ns homepage.core
  (:require 
  	[reagent.core :as reagent :refer [atom]]
  	[secretary.core :as secretary :refer-macros [defroute]]
  	[homepage.views.main :refer [main-view]]
  	[devtools.core :as devtools]
  	[cljs-react-material-ui.core :refer [get-mui-theme]]
    [cljs-react-material-ui.reagent :refer [mui-theme-provider]]))

(devtools/install!)
(enable-console-print!)

(defroute home-path "/" []
	(println "at root page"))

(reagent/render-component [mui-theme-provider {:mui-theme (get-mui-theme)}
   [main-view]]
                          (. js/document (getElementById "app")))