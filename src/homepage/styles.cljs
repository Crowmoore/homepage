(ns homepage.styles)

(def banner
	{:display "flex"
	 :flex-direction "column"
	 :align-items "center"
	 :height "500px"
	 :color "white"
	 :background-color "#00BCD4"})

(def centered
	{:display "flex"
	 :justify-content "center"})

(def nav-row
	{:display "flex"
	 :flex-direction "row"})

(def nav-link
	{:font-size "medium"})