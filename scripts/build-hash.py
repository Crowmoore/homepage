import sys
import os

def replace():
	path = "resources/public/index.html"
	content = open(path, 'r').read()
	new_content = open(path, 'w')

	changes = content.replace("build-tag", os.environ['BITBUCKET_COMMIT'])

	new_content.write(changes)
	new_content.close()

replace()